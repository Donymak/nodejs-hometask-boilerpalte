const { BaseRepository } = require('./baseRepository');

class FighterRepository extends BaseRepository {
    constructor() {
        super('fighters');
    }

    getOneByName(regEx) {
        return this.dbContext.find(fighter => regEx.test(fighter.name)).value();
    }

}

exports.FighterRepository = new FighterRepository();