const FighterService = require('../services/fighterService');
const {EntityNotFoundError} = require("../errors/EntityNotFoundError");

class FighterController {

    getAll(req, res) {
        const fighters = FighterService.getAll()
        res.status(200).json(fighters)
    }

    getOne(req, res) {
        const fighter = FighterService.search({id: req.params.id});
        if (fighter) {
            return res.status(200).json(fighter);
        }

        res.status(404).json({error: "Fighter not found"});
    }

    create(req, res, next) {
        try  {
            const fighter = FighterService.create(req.body);
            next({status: 200, response: fighter});
        } catch (err) {
            next({status: 400, message: err.message});
        }
    }

    update(req, res, next) {
        try {
            const updated = FighterService.update(req.params.id, req.body);
            next({status: 200, response: updated});
        } catch (err) {
            if (err instanceof EntityNotFoundError) {
                return next({status: 404, message: err.message});
            }

            next({status: 400, message: err.message});
        }
    }

    delete(req, res) {
        const deleted = FighterService.delete(req.params.id);
        res.status(200).json(deleted);
    }
}

module.exports = new FighterController()