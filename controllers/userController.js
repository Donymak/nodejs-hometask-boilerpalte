const UserService = require('../services/userService');
const {EntityNotFoundError} = require("../errors/EntityNotFoundError");

class UserController {
    getAll(req, res) {
        const users = UserService.getAll();
        res.status(200).json(users);
    }

    getOne(req, res) {
        const user = UserService.search({id: req.params.id});

        if (user) {
            return res.status(200).json(user);
        }

        res.status(404).json({error: "User not found"});
    }

    create(req, res, next) {
        try {
            const user = UserService.create(req.body);
            next({status: 200, response: user});
        } catch (err) {
            next({status: 400, message: err.message});
        }
    }

    update(req, res, next) {
        try {
            const updated = UserService.update(req.params.id, req.body);
            next({status: 200, response: updated});
        } catch (err) {
            if (err instanceof EntityNotFoundError) {
                return next({status: 404, message: err.message});
            }

            next({status: 400, message: err.message});
        }
    }

    delete(req, res) {
        const deleted = UserService.delete(req.params.id)
        res.status(200).json(deleted)
    }
}

module.exports = new UserController();