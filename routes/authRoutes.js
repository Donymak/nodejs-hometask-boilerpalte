const {Router} = require('express');
const AuthService = require('../services/authService');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();


router.post('/login', (req, res, next) => {

    try {
        const user = AuthService.login(req.body);
        next({status: 200, response: user});
    } catch (err) {
        next({status: 401, message: err.message})
    }
}, responseMiddleware);

module.exports = router;