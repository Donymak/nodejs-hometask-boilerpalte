const {UserRepository} = require('../repositories/userRepository');
const {FighterRepository} = require("../repositories/fighterRepository");
const {EntityNotFoundError} = require("../errors/EntityNotFoundError");

class UserService {

    getAll() {
        return UserRepository.getAll();
    }

    search(search) {
        return UserRepository.getOne(search);
    }

    create(user) {
        if (UserRepository.getOne({phoneNumber: user.phoneNumber})) {
            throw new Error("User with given phone number already exists");
        } else if (UserRepository.getOne({email: user.email})) {
            throw new Error("User with given email already exists")
        }
        return UserRepository.create(user);
    }

    update(id, dataToUpdate) {
        if (!UserRepository.getOne({id})) {
            throw new EntityNotFoundError("User is not found");
        }

        if (UserRepository.getOne({phoneNumber: dataToUpdate.phoneNumber})) {
            throw new Error("User with given phone number already exists");
        } else if (UserRepository.getOne({email: dataToUpdate.email})) {
            throw new Error("User with given email already exists")
        }

        return UserRepository.update(id, dataToUpdate);
    }

    delete(id) {
        return UserRepository.delete(id)

    }
}

module.exports = new UserService();