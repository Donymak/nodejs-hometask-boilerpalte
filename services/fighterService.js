const { FighterRepository } = require('../repositories/fighterRepository');
const {fighter} = require("../models/fighter");
const { EntityNotFoundError } = require("../errors/EntityNotFoundError");

class FighterService {
    getAll() {
        return FighterRepository.getAll();
    }

    search(search) {
        return FighterRepository.getOne(search);
    }

    create(fighter) {
        if (FighterRepository.getOneByName(new RegExp(`^${fighter.name}$`, 'i'))) {
            throw new Error("Fighter with given name already exists");
        } else if (!fighter.name || fighter.name && fighter.name.length === 0) {
            throw new Error("No fighter name or it is empty");
        }
        return FighterRepository.create(fighter);
    }

    update(id, dataToUpdate) {
        if (!FighterRepository.getOne({id})) {
            throw new EntityNotFoundError("Fighter is not found");
        }

        if (FighterRepository.getOneByName(new RegExp(`^${dataToUpdate.name}$`, 'i'))) {
            throw new Error("Fighter with given name already exists");
        }

        return FighterRepository.update(id, dataToUpdate)
    }

    delete(id) {
        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();