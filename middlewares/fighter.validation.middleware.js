const {fighter} = require('../models/fighter');
const {validateRequestBodyKeys} = require("../utils/validation");

const createFighterValid = (req, res, next) => {
    const reqBody = req.body;

    if (!reqBody.health) {
        reqBody.health = fighter.health;
    }

    try {
        _validateFighter(reqBody, res);

        next();
    } catch (err) {
        res.status(400).json({error: true, message: err.message});
    }
}

const updateFighterValid = (req, res, next) => {

    try {
        _validateFighter(req.body, res);

        next();
    } catch (err) {
        res.status(400).json({error: true, message: err.message});
    }
}

const _validateFighter = (actualFighter) => {

    if (Object.keys(actualFighter).length === 0) {
        throw new Error("Object can't be empty")
    }

   validateRequestBodyKeys(actualFighter, fighter)

    if (actualFighter.id) {
        throw new Error("Id mustn't be in body")
    }

    if (actualFighter.power <= 1 || actualFighter.power >= 100) {
        throw new Error("Power must be from 1 to 100");
    } else if (actualFighter.defense <= 1 || actualFighter.defense >= 10) {
        throw new Error("Defence must be from 1 to 10");
    } else if (actualFighter.health <= 80 || actualFighter.health >= 120) {
        throw new Error("Health must be from 80 to 120");
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;