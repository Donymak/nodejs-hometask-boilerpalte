const {user} = require('../models/user');
const {validateRequestBodyKeys} = require("../utils/validation");
const {fighter} = require("../models/fighter");
const createUserValid = (req, res, next) => {

    try {
        _validateUser(req.body, res);

        next();
    } catch (err) {
        res.status(400).json({error: true, message: err.message});
    }
}

const updateUserValid = (req, res, next) => {

    try {
        _validateUser(req.body, res);

        next();
    } catch (err) {
        res.status(400).json({error: true, message: err.message});
    }
}
const _phoneMask = /^\+380([0-9]{9})$/;
const _gmailMask = /^[a-z0-9](\.?[a-z0-9]){5,}@gmail\.com$/;

const _validateUser = (actualUser) => {
    if (actualUser.id) {
        throw new Error("Id mustn't be in body");
    }

    validateRequestBodyKeys(actualUser, user);

    if (!actualUser.email.match(_gmailMask)) {
        throw new Error("Email is incorrect");
    } else if (actualUser.password.length < 3) {
        throw new Error("Password must contain at least 3 characters");
    } else if (!actualUser.phoneNumber.match(_phoneMask)) {
        throw new Error("Phone number is incorrect");
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;