const responseMiddleware = (obj, req, res, _) => {
    if (obj.response) {
        return res.status(obj.status).json(obj.response)
    }

    res.status(obj.status).json({error: true, message: obj.message})

}

exports.responseMiddleware = responseMiddleware;