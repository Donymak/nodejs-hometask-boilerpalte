const validateRequestBodyKeys = (actual, expected) => {
    Object.keys(actual).forEach(key => {
        if (!Object.keys(expected).includes(key)) {
            throw new Error(`${key} is not allowed in request body`)
        }
    })
}

exports.validateRequestBodyKeys = validateRequestBodyKeys;