class EntityNotFoundError extends Error {
    constructor(message) {
        super(message);
    }
}

exports.EntityNotFoundError = EntityNotFoundError;